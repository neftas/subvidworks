# Subvidworks

<b>Subvidworks</b> is a Qt-based program to quickly rename video and subtitle files so that they will be automatically played together when used by a video player.

## Installation

Subvidworks is currently available for Linux and Mac OS X. 

* If you are on Ubuntu (>= 12.04) or a derivative, you can download and install a [Debian package](https://github.com/Neftas/subvidworks/releases). Alternatively, you can add my Personal Package Archive from [Launchpad](https://launchpad.net/~srvandenakker/+archive/ubuntu/ppa). This way you will be sure to receive updates and bug fixes when they become available. To add the PPA, open a terminal and enter:

        sudo add-apt-repository ppa:srvandenakker/ppa
        sudo apt-get update
        sudo apt-get install python-subvidworks
        
* If you are on Mac OS X 10.6 or later, you can download a [.zip file](https://github.com/Neftas/subvidworks/releases). Unpack the file and drag & drop the resulting application bundle into your Applications folder.

## Help

Help is available in the application and can be reached by either pressing <kbd>F1</kbd> or by going to _Help > Help..._ in the menu bar.

## Bugs and feature requests

Please report any bugs you find [here on GitHub](https://github.com/Neftas/subvidworks/issues). To make things easier, the application also offers to send a bug report by email. Please, don't forget to include the log files. These are automatically grouped in a .zip file in your local `Downloads` folder when you choose to email a bug report, but are otherwise located in `~/.config/subvidworks`.

If you are missing a feature, don't hesitate to write a [feature request](https://github.com/Neftas/subvidworks/issues).
