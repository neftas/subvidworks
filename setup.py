try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'name': 'subvidworks',
    'description': 'Subvidworks matches video and subtitle filenames',
    'author': 'Stefan van den Akker',
    'author_email': 'srvandenakker@gmail.com',
    'url': 'https://github.com/Neftas/subvidworks',
    'download_url': 'https://code.launchpad.net/~srvandenakker/+recipe/subvidworks-daily',
    'version': '0.2',
    'license': 'GPL v3',
    'packages': ['subvidworks'],
    'entry_points': {'gui_scripts': ['subvidworks = subvidworks.subvidworks:main']},
    'data_files': [('share/applications/', ['data/subvidworks.desktop']), ('share/python-subvidworks/data/', ['docs/help.html', 'docs/help_details.html', 'docs/help_options.html', 'docs/help_style.css']), ('share/icons/hicolor/128x128/apps/', ['data/icons/128x128/subvidworks.png']), ('share/icons/hicolor/64x64/apps/', ['data/icons/64x64/subvidworks.png'])],
    'classifiers': ['Development Status :: 4 - Beta', 'Intended Audience :: End Users/Desktop', 'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)', 'Operating System :: POSIX :: Linux'],
    'long_description': """Subvidworks is a Qt-based program to quickly rename video and subtitle files so that they will be automatically played together when used by a video player."""
}

setup(**config)

