#!/usr/bin/python

# Copyright 2014 Stefan van den Akker
#
# This file is part of Subvidworks.
#
# Subvidworks is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# Subvidworks is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Subvidworks. If not, see http://www.gnu.org/licenses/.


# TO DO
##################################################
# - create what's this
# - create a subvidworks icon
# - make changes to make it work on Mac OS X


import sys
import os
import glob
import fnmatch
import re
import logging
import json
import itertools
import time
import zipfile
from PyQt4 import QtGui, QtCore, QtWebKit


# Helper functions and variables
##################################################

VERSION = "0.2.1"

PLATFORM = sys.platform

FILE_EXTS = {
            "video": ["mkv", "mp4", "avi", "m4v", "divx", "xvid", "mpg", "mpeg",
            "flv", "vob", "mov", "wmv"],
            "subtitle": ["srt", "txt", "sub", "ssa"]
            }

TEMP_DIR = ".ready"

NO_FILES_PROCESSED = 0

def get_config_folder():
    """
    Returns the folder where the setting are kept. If the OS is not
    supported, exits the program with an error message.
    """
    if PLATFORM.startswith(("linux", "darwin")):
        return os.path.join(os.path.expanduser("~"), ".config", "subvidworks")
    else:
        logging.critical("OS {!r} is not supported. Aborting.".format(
            PLATFORM))
        sys.exit("OS {!r} is currently not supported.".format(PLATFORM))

# set up directory for subvidworks
conf_folder = get_config_folder()
if not os.path.exists(conf_folder):
    try:
        os.makedirs(conf_folder)
    except IOError:
        raise IOError("Could not create {}".format(conf_folder))

# find flag for setting log severity level
loglevel = ""

for arg in sys.argv[1:]:
    if arg.startswith("--log="):
        loglevel = arg[arg.find("=")+1:]

numeric_level = getattr(logging, loglevel.upper(), None)
if not isinstance(numeric_level, int):
    numeric_level = logging.DEBUG
    # raise ValueError("Invalid log level: %s" % loglevel)

# make sure numeric_level is an int, not just None
assert numeric_level

# set up logging
log_location = os.path.join(get_config_folder(), ".subvidworks_log")
assert os.path.abspath(log_location), "{} is not an absolute path".format(
    log_location)
FORMAT = "[%(asctime)s : %(lineno)4s : %(funcName)25s() : %(levelname)s] %(message)s"
logging.basicConfig(filename=log_location, filemode="w", level=numeric_level,
    format=FORMAT, datefmt="%F %T")

logging.debug("No valid logging severity level set by user: {!r}".format(
    loglevel))

def save_filenames_to_file(dict_filelocations, path=None):
    """Save the current filenames in the renamed_files dictionary to disk."""
    if path is None:
        path = os.path.join(get_config_folder(), ".subvidworks_filenames")
    try:
        with open(path, "w") as f:
            json.dump(dict_filelocations, f)
    except IOError:
        logging.error("Could not save the changed filenames to {}".format(
            path))

def get_season_and_episode(filename):
    """Extracts the season and episode numbers out of a filename. Returns tuples
    with the season number, episode number and the complete match, like
    ('04', '21', '04x21'). Incomplete matches are represented as
    (None, '21', 'Ep21'). Returns (None, None, None) if neither a season number
    nor an episode number can be found.
    >>> get_season_and_episode("")
    (None, None, None)
    >>> get_season_and_episode("how.i.met.your.mother.s02e21.720p.hdtv.x264-ctu.mkv")
    ('2', '21', 's02e21')
    >>> get_season_and_episode("24 [6x09] 02.00 PM - 03.00 PM (XviD asd).avi")
    ('6', '9', '6x09')
    >>> get_season_and_episode("Band.Of.Brothers.Ep10.2001.1080p.BluRay.x264.anoXmous_.mp4")
    (None, '10', 'Ep10')
    >>> get_season_and_episode("24 [6x09] 02.00 PM - 03.00 PM (XviD asd).avi")
    ('6', '9', '6x09')
    """
    pattern = (r"(?i)(?:[s][eason]*\s?(?P<s>\d{1,2}).*?)?[e][pisode]*\s?"
        r"(?P<e>\d{1,2})|(?P<sa>\d{1,2})x(?P<ea>\d{1,2})")

    match = re.search(pattern, filename)

    if match:
        result = match.groups()
        complete_match = match.group(0)
        # remove all Nones
        result_trimmed = filter(None, result)
        # strip leading zeroes
        result_trimmed = tuple(
            [strip_leading_zero(elem) for elem in result_trimmed])
        if len(result_trimmed) > 1:
            return result_trimmed + (complete_match,)
        else:
            if result[0] is None and result[2] is None:
                logging.warning("File {!r}: season info is missing".format(
                    filename))
                return (None,) + result_trimmed + (complete_match,)
            elif result[1] is None and result[3] is None:
                logging.warning("File {!r}: episode info is missing".format(
                    filename))
                return result_trimmed + (None, complete_match)
    else:
        return (None, None, None)

def get_keyword(filename):
    """
    Extracts the longest possible keyword out of a filename, before the
    season and episode data. Takes as input a filename and returns the most
    suitable keyword.
    >>> get_keyword("")

    >>> get_keyword("how.i.met.your.mother.s02e21.720p.hdtv.x264-ctu.mkv")
    'mother'
    >>> get_keyword("Planet.Earth.e11.Ocean.Deep.2006.1080p.HDDVD.x264.anoXmous_.mp4")
    'Planet'
    >>> get_keyword("24 [1x21] (XviD asd).avi")
    '24'
    """

    __, __, complete_match = get_season_and_episode(filename)

    if complete_match is not None:
        end = filename.find(complete_match)
        filename = filename[:end]

    pattern = r"(?i)(\w+)"
    match = re.findall(pattern, filename)
    longest = ""

    if match:
        for word in match:
            if check_for_keywords(word):
                continue
            if len(word) > len(longest):
                longest = word

    logging.debug("From filename {!r} keyword {!r} was extracted".format(
        filename, longest))
    return longest or None

def check_for_keywords(s, percent_match=80.0):
    """Checks if a possible keyword is in fact used for describing 
    the season and episode of a series. Returns True if the word is
    more than percent_match likely to be such a word, False otherwise.
    """
    if not s:
        return False
    season_check = [c in set("season") for c in s]
    result1 = (season_check.count(True) /
        float(len(season_check)) * 100 >= percent_match)
    episode_check = [c in set("episode") for c in s]
    result2 = (episode_check.count(True) /
        float(len(episode_check)) * 100 >= percent_match)
    return any((result1, result2))

def case_insensitive_glob(pattern):
    """Returns a glob pattern that is case insensitive.
    >>> case_insensitive_glob("")
    ''
    >>> case_insensitive_glob("HeAt")
    '[hH][eE][aA][tT]'
    """
    def either(c):
        return "[{0}{1}]".format(c.lower(), c.upper()) if c.isalpha() else c
    return "".join([either(c) for c in pattern])

def change_vid_sub_filenames(vid_dir, sub_dir, vid_list, sub_list,
    one_directory, multiple_dirs, new_target, rename=None):
    renamed_files = {"video": {}, "sub": {}}

    # in order to make search multiple folders to work, we need to make 
    # dest_dir flexible
    if new_target:
        dest_dir_vid = dest_dir_sub = new_target
    elif one_directory:
        dest_dir_vid = dest_dir_sub = vid_dir
    else:
        dest_dir_vid = vid_dir
        dest_dir_sub = sub_dir

    if rename:
        # decide on most suitable divider between rename and 
        # season and episode information
        div = suitable_divider(rename)

        temp_rename_file = rename

    for f_video in vid_list:
        assert os.path.isabs(f_video)
        head_video, tail_video = os.path.split(f_video)
        fn_tail_video, ext_tail_video = os.path.splitext(tail_video)
        s_video, e_video, match_video = get_season_and_episode(tail_video)
        for f_sub in sub_list:
            assert os.path.isabs(f_sub)
            head_sub, tail_sub = os.path.split(f_sub)
            fn_tail_sub, ext_tail_sub = os.path.splitext(tail_sub)
            s_sub, e_sub, match_sub = get_season_and_episode(tail_sub) 
            if (s_video == s_sub) and (e_video == e_sub):
                # keep dest_dir_vid pure
                org_dest_dir_vid = dest_dir_vid
                # rename subtitle filename to match video filename
                # put the new files in a separate folder named .ready/
                if rename:
                    temp_rename_file = rename
                    temp_rename_dir = "{0}{1}S{2}".format(temp_rename_file, div,
                        prepend_zero(s_video))
                    temp_rename_file = "{0}{1}S{2}E{3}".format(temp_rename_file,
                        div, prepend_zero(s_video), prepend_zero(e_video))
                    
                    # head_video = (head_video, dest_dir_vid)[bool(new_target)]
                    if multiple_dirs and new_target:
                        # parent_dir_vid = os.path.split(head_video)[1]
                        dest_dir_vid = os.path.join(dest_dir_vid, temp_rename_dir)
                        print "Dest dir vid:", dest_dir_vid
                    else:
                        dest_dir_vid = head_video
                    # rename video files (and make the extension is lowercase)
                    v_dst = os.path.join(dest_dir_vid,
                        temp_rename_file + ext_tail_video.lower())
                    print "v_dest:", v_dst

                    # rename sub files (and extension to lowercase)
                    # if multiple dirs are used, copy the sub file to the
                    # appropriate video dir
                    dest_dir_sub = (head_sub, dest_dir_vid)[multiple_dirs
                        and (one_directory or bool(new_target))]
                    s_dst = os.path.join(
                        dest_dir_sub, temp_rename_file + ext_tail_sub.lower())

                    renamed_files = final_destination(f_video, v_dst,
                        renamed_files, "video")
                    renamed_files = final_destination(f_sub, s_dst,
                        renamed_files, "sub")
                else:
                    if new_target:
                        if multiple_dirs:
                            parent_dir_vid = os.path.split(head_video)[1]
                            dest_dir_vid = os.path.join(dest_dir_vid,
                                    parent_dir_vid)
                            print "Dest dir vid:", dest_dir_vid
                        v_dst = os.path.join(
                            dest_dir_vid, fn_tail_video + ext_tail_video.lower())
                        renamed_files = final_destination(f_video, v_dst,
                            renamed_files, "video")

                    dest_dir_sub = (head_sub, dest_dir_vid)[multiple_dirs
                        and one_directory]
                    dest_path = os.path.join(
                        dest_dir_sub, fn_tail_video + ext_tail_sub.lower())
                    assert os.path.isabs(dest_path)

                    renamed_files = final_destination(f_sub, dest_path,
                        renamed_files, "sub")
                # reset dest_dir_vid
                dest_dir_vid = org_dest_dir_vid
                break

    # save dict to disk
    save_filenames_to_file(renamed_files)

    return renamed_files

def final_destination(org_filename, dest_filename, renamed_files, filetype):
    org_head, org_tail = os.path.split(org_filename)
    dest_head, dest_tail = os.path.split(dest_filename)

    if org_filename == dest_filename:
        return renamed_files
    else:
        pattern = re.compile(r"\((\d+)\)(?=\.\w{1,3}$)")

        match = re.search(pattern, dest_tail)
        if match:
            repeat_number = match.group(1)
        else:
            repeat_number = "0"

        result = list()
        for f in renamed_files[filetype].values():
            path, fn = os.path.split(f)
            result.append(fn)

        # make sure the destination directory exists: if not, create it first
        # created_dirs = list()
        # if not os.path.exists(dest_head):
        #     os.makedirs(dest_head)
        #     created_dirs.append(dest_head)
        if os.path.exists(dest_head):
            dest_list = os.listdir(dest_head) + result
        else:
            dest_list = result

        temp_filename = dest_tail
        while temp_filename in dest_list:
            filename, ext = os.path.splitext(temp_filename)
            if repeat_number:
                repeat_number = str(int(repeat_number) + 1)
                begin = filename.rfind("(")
                temp_filename = filename[:begin] + "({0}){1}".format(repeat_number, ext)
            else:
                repeat_number = "1"
                temp_filename = filename + "({0}){1}".format(repeat_number, ext)

        final_dest = os.path.join(dest_head, temp_filename)

        renamed_files[filetype][org_filename] = final_dest
        logging.debug("'renamed_files[{!r}][{!r}] = {!r}'".format(
            filetype, org_filename, final_dest))

    save_filenames_to_file(renamed_files)

    return renamed_files

def move_files(dict_filelocations):
    """Moves files from the current location to their destination location."""
    global NO_FILES_PROCESSED
    NO_FILES_PROCESSED = 0

    for filetype in dict_filelocations:
        for src, dst in dict_filelocations[filetype].items():
            dst_dir, __ = os.path.split(dst)
            if not os.path.exists(dst_dir):
                os.makedirs(dst_dir)
                logging.debug("Created directory: {!r}".format(dst_dir))
            os.rename(src, dst)
            logging.debug("Moved {0} > {1}".format(src, dst))
            NO_FILES_PROCESSED += 1

def replace_files(renamed_files, new_target):
    """Place files back in their original directory, and rename them to their
    old filenames."""
    global NO_FILES_PROCESSED
    NO_FILES_PROCESSED = 0

    for filetype in renamed_files:
        for dst, src in renamed_files[filetype].items():
            os.rename(src, dst)
            logging.debug("Replaced {0} > {1}".format(src, dst))
            NO_FILES_PROCESSED += 1
            # remove empty directories after replacing files
            if new_target:
                path = os.path.dirname(src)
                if not os.listdir(path):
                    os.rmdir(path)

def create_work_dir(path):
    """Creates a directory TEMP_DIR at location path."""
    if not os.path.exists("{0}/{1}".format(path, TEMP_DIR)):
        os.makedirs("{0}/{1}".format(path, TEMP_DIR))

def prepend_zero(s):
    """ Prepends a zero to a string number s.
    >>> prepend_zero("")
    '00'
    >>> prepend_zero("1")
    '01'
    >>> prepend_zero("21")
    '21'
    >>> prepend_zero("121")
    '121'
    """
    if not s:
        return "00"
    if not s.isdigit():
        return s
    if len(s) == 1:
        return '0' + s
    return s

def strip_leading_zero(s):
    """Strips the leading zero of a string s.
    >>> strip_leading_zero("")
    ''
    >>> strip_leading_zero("01")
    '1'
    >>> strip_leading_zero("10")
    '10'
    """
    if not s:
        return s
    if s.startswith('0'):
        return s[1:]
    return s

def suitable_divider(filename, dividers="+-._ "):
    """Returns the most used divider based on the given filename.
    # >>> suitable_divider("")
    """
    if filename.endswith(tuple(dividers)):
        return ""
    winner = " "
    for div in dividers:
        if filename.count(div) > filename.count(winner):
            winner = div
    return winner

def choose_dominating_files(file_list):
    """Requires as input a list of filename strings. Returns a list that
    contains only the strings that start alike. It uses season and episode
    information as delimiter, so lists without that information will be
    returned unchanged. If the input is an empty list, it will return an
    empty list.
    >>> file_list = ["a.name.s01.e01", "a.name.s01.e02", "yes.a.name.s01e09"]
    >>> choose_dominating_files(file_list)
    ['a.name.s01.e01', 'a.name.s01.e02']
    >>> choose_dominating_files([])
    []
    """
    if not file_list:
        return file_list

    d = dict()

    for f in file_list:
        __, f = os.path.split(f)
        break_out = False
        for pattern in d:
            if f.startswith(pattern):
                d[pattern] += 1
                break_out = True
                break
        if not break_out:
            v1, v2, complete_match = get_season_and_episode(f)
            if not complete_match:
                continue
            end = f.find(complete_match)
            f = f[:end]
            d[f] = d.get(f, 0) + 1

    # get the string of the filename with the most occurrences in the list
    if d:
        greatest = max(d.items(), key=lambda x: x[1])[0]
        return [f for f in file_list if os.path.split(f)[1].startswith(greatest)]
    return []

def create_qframe():
    """Creates a sunken horizontal QFrame."""
    frame = QtGui.QFrame()
    frame.setFrameShape(QtGui.QFrame.HLine)
    frame.setFrameShadow(QtGui.QFrame.Sunken)
    return frame

def create_spacer_widget():
    """Create a spacer widget."""
    spacer = QtGui.QWidget()
    spacer.setMinimumSize(100, 12)
    spacer.setSizePolicy(QtGui.QSizePolicy.Preferred,
        QtGui.QSizePolicy.Expanding)
    # spacer.setStyleSheet("background-color: red")
    return spacer

def prettify_src_dst_info(dict_filelocations, type_file):
    """Return a string in HTML that prints where files are going to be moved."""
    temp_s = ""

    for src, dst in sorted(dict_filelocations[type_file].items()):
        # escape HTML special characters
        html_escape_table = {
        "&": "&amp;",
        '"': "&quot;",
        "'": "&apos;",
        ">": "&gt;",
        "<": "&lt;",
        }
        src = "".join(html_escape_table.get(c, c) for c in src)
        dst = "".join(html_escape_table.get(c, c) for c in dst)
        temp_s = temp_s + ("<p><span class=\"red\">{}</span>"
            "<br /><span class=\"green\">--> {}</span></p>".format(src, dst))

    result = """
    <!DOCTYPE html>
    <html>
    <head>
        <style type="text/css">
        span {{ font-family: Droid Sans Mono, monospace; }}
        .red {{ color: red; }}
        .green {{ color: green; }}
        </style>
    </head>
    <body>
        {}
    </body>
    </html>
    """.format(temp_s)

    return result if temp_s else temp_s

def all_combinations_exts(ext):
    """Take an extension string ext and return a list of all possible upper
    and lowercase possibilities.
    >>> all_combinations_exts("")
    []
    >>> all_combinations_exts(".ext")
    ['.EXT', '.EXt', '.ExT', '.Ext', '.eXT', '.eXt', '.exT', '.ext']
    >>> all_combinations_exts("mp4")
    ['MP4', 'Mp4', 'mP4', 'mp4']
    """
    if not ext:
        logging.warning("{!r} was an empty string: function {!r} returned "
            "empty list".format(ext, "all_combinations_exts"))
        return list()
    temp = ["".join(x) for x in itertools.product(*zip(ext.upper(), ext.lower()))]
    result = list()
    for elem in temp:
        if elem not in result:
            result.append(elem)
    logging.debug("Converted {!r} to list {!r}".format(ext, result))
    return result

def ext_formatting(type_file):
    """Create proper extension strings like *.ext and *.mp4 so they can be used
    by QFileDialog."""
    formatted_file_exts = ""
    l = FILE_EXTS[type_file]
    for i, elem in enumerate(l):
        if i < len(l) - 1:
            elem = "*." + elem + " "
        else:
            elem = "*." + elem
        formatted_file_exts += elem
    return formatted_file_exts

def go_to_github():
    """Open a browser tab with GitHub issues for Subvidorks."""
    logging.debug("User reports error on GitHub")
    url = "https://github.com/Neftas/subvidworks/issues"
    QtGui.QDesktopServices.openUrl(QtCore.QUrl(url, QtCore.QUrl.StrictMode))

def send_error_report():
    """Open the native email client and compose an email. On Linux, the email
    will automatically contain an attached zipfile with the logs. On other
    platforms the user will have to attach it themselves. The zipfile will
    be created in the Downloads folder of the user."""
    logging.debug("User sends error via email")
    log_file = os.path.join(get_config_folder(), ".subvidworks_log")
    filenames_file = os.path.join(get_config_folder(), ".subvidworks_filenames")
    zip_location = os.path.join(os.path.expanduser("~"), "Downloads")
    zip_fn = os.path.join(zip_location, "subvidworks_logs_{0}.zip".format(
        time.strftime("%F_%T")))
    with zipfile.ZipFile(zip_fn, "w") as zipf:
        zipf.write(log_file, "subvidworks_log")
        zipf.write(filenames_file, "subvidworks_filenames")
    QtGui.QDesktopServices.openUrl(QtCore.QUrl(
        "mailto:srvandenakker.dev@gmail.com?subject=Error report for "
        "Subvidworks created {0}&body=I just encountered the following error:"
        "\n\n[Description of what happened, and what was expected to "
        "happen: please be as detailed as possible. Also, don't forget to "
        "attach the logfiles. Subvidworks automatically created a zipfile in "
        "your Downloads folder {2!r} with the logfiles.]"
        "&attachment={1}".format(time.strftime("%F %T"), zip_fn, zip_location)))

# Layout
##################################################

class MainWindow(QtGui.QMainWindow):
    """Main application window."""
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUI()
        self.renamed_files_path = ""
        self.rename_text = ""
        self.vid_keyword = ""
        self.sub_keyword = None
        self.multiple_dir_flag = False
        self.copy_flag = False
        self.details_flag = False
        self.rename_flag = False
        self.renamed_files = {"video": {}, "sub": {}}
        self.setup_dict_filelocations()
        # initialize lists that keep track of the removed items
        self.removed_video_items = list()
        self.removed_sub_items = list()
        # lists of files
        self.sub_file_list = list()
        self.video_file_list = list()
        self.org_search_directories = False
        # new target directory
        self.new_target_directory = ""
        self.new_target_flag = False
        # initialize directories
        self.video_directory = ""
        self.sub_directory = ""

    def setup_dict_filelocations(self):
        self.renamed_files_path = os.path.join(get_config_folder(),
            ".subvidworks_filenames")
        assert os.path.abspath(self.renamed_files_path), (
            "{} is not an absolute path".format(self.renamed_files_path))

        # load .subvidworks_filenames
        try:
            with open(self.renamed_files_path, "r") as f:
                self.renamed_files = json.load(f)
                logging.info("'self.renamed_files' loaded from {}".format(
                    self.renamed_files_path))
        except:
            logging.info("renamed_files file not found at {0}: created an "
                "empty file".format(self.renamed_files_path))
            # if file doesn't exist, create empty file
            with open(self.renamed_files_path, "w") as f:
                pass

    def setupUI(self):
        self.setGeometry(100, 100, 600, 200)
        self.setWindowTitle("Subvidworks v{0}".format(VERSION))
        if PLATFORM.startswith("linux"):
            icon_file = os.path.join("/", "usr", "share", "icons", "hicolor",
                "128x128", "apps", "subvidworks.png")
        elif PLATFORM.startswith("darwin"):
            icon_file = os.path.join("Applications", "Subvidworks.app",
            "Contents", "MacOS", "subvidworks.png")
        else:
            # IMPLEMENT FOR OTHER PLATFORMS
            logging.critical("OS {!r} not supported".format(PLATFORM))
            sys.exit("OS {!r} not supported".format(PLATFORM))
        self.setWindowIcon(QtGui.QIcon(icon_file))

        # create filemenu
        menubar = self.menuBar()

        filemenu = menubar.addMenu("&File")
        help_menu = menubar.addMenu("&Help")

        exit_action = QtGui.QAction("&Exit", self)
        exit_action.setShortcut("Ctrl+Q")
        exit_action.triggered.connect(self.close)
        filemenu.addAction(exit_action)

        donate_action = QtGui.QAction("&Donate...", self)
        donate_action.setToolTip("If you find Subvidworks to be useful "
            "please consider\na donation to ensure it further development")
        donate_action.triggered.connect(self.donate_dialog)
        help_menu.addAction(donate_action)

        error_report_action = QtGui.QAction("&Report bug...", self)
        error_report_action.triggered.connect(self.bug_report)
        help_menu.addAction(error_report_action)

        help_action = QtGui.QAction("&Help...", self)
        help_action.setShortcut("F1")
        help_action.triggered.connect(self.help_window)
        help_menu.addAction(help_action)

        aboutqt = QtGui.QAction("Abou&t Qt...", self)
        aboutqt.triggered.connect(self.aboutqt)
        help_menu.addAction(aboutqt)

        about_action = QtGui.QAction("&About Subvidworks...", self)
        about_action.triggered.connect(self.about)
        help_menu.addAction(about_action)

        self.main_widget = QtGui.QWidget(self)

        self.groupbox_stylesheet = """
            QGroupBox { border: 1px inset lightgrey;
                        border-radius: 5px;
                        margin-top: 10px;
                        font-weight: bold; }
            QGroupBox::title { subcontrol-origin: margin;
                               subcontrol-position: top;
                               /*left: 40px;*/
                               padding:0 3px 0 3px; }
        """

        self.label_stylesheet = """
            QLabel { border: 1px inset lightgrey;
                        border-radius: 5px;
                        margin-top: 10px; }
        """

        groupbox_files = QtGui.QGroupBox()
        groupbox_files.setTitle("Select files")
        groupbox_files.setStyleSheet(self.groupbox_stylesheet)

        # get video file sample
        video_label = QtGui.QLabel("<b>Select video file</b>")
        video_button = QtGui.QPushButton("&Browse...", self)
        video_button.setToolTip("Select a video filename")
        video_button.resize(video_button.sizeHint())
        video_button.setSizePolicy(QtGui.QSizePolicy.Fixed,
            QtGui.QSizePolicy.Minimum)
        video_button.clicked.connect(lambda: self.get_filename("video"))
        self.file_label_video = QtGui.QLabel("")
        self.file_label_video.setStyleSheet("color: grey")

        # get subtitle file sample
        sub_label = QtGui.QLabel("<b>Select subtitle file</b>")
        sub_button = QtGui.QPushButton("Br&owse...", self)
        sub_button.setToolTip("Select a subtitle filename")
        sub_button.resize(sub_button.sizeHint())
        sub_button.setSizePolicy(QtGui.QSizePolicy.Fixed,
            QtGui.QSizePolicy.Minimum)
        sub_button.clicked.connect(lambda: self.get_filename("subtitle"))
        self.file_label_sub = QtGui.QLabel("")
        self.file_label_sub.setStyleSheet("color: grey")

        video_hbox = QtGui.QHBoxLayout()
        video_hbox.addWidget(video_button)
        video_hbox.addWidget(self.file_label_video)

        video_vbox = QtGui.QVBoxLayout()
        video_vbox.addWidget(video_label)
        video_vbox.addLayout(video_hbox)

        sub_hbox = QtGui.QHBoxLayout()
        sub_hbox.addWidget(sub_button)
        sub_hbox.addWidget(self.file_label_sub)

        sub_vbox = QtGui.QVBoxLayout()
        sub_vbox.addWidget(sub_label)
        sub_vbox.addLayout(sub_hbox)

        sep = create_qframe()
        self.target_button_checkbox = QtGui.QCheckBox(
            "Select &custom target directory", self)
        self.target_button_checkbox.stateChanged.connect(self.enable_target_button)
        self.target_button_checkbox.setToolTip("Select a directory to "
            "save the renamed video and subtitle files to")

        self.target_directory_button = QtGui.QPushButton("Bro&wse...", self)
        self.target_directory_button.setToolTip("Select a target folder")
        self.target_directory_button.clicked.connect(self.select_target_dir)
        self.target_directory_button.setEnabled(False)
        self.target_directory_label = QtGui.QLabel("")
        self.target_directory_label.setStyleSheet("color: grey")
        target_dir_hbox = QtGui.QHBoxLayout()
        target_dir_hbox.addWidget(self.target_directory_button)
        target_dir_hbox.addWidget(self.target_directory_label)
        target_dir_hbox.addStretch(1)

        files_vbox = QtGui.QVBoxLayout()
        files_vbox.addLayout(video_vbox)
        files_vbox.addLayout(sub_vbox)
        files_vbox.addWidget(sep)
        files_vbox.addWidget(self.target_button_checkbox)
        files_vbox.addLayout(target_dir_hbox)

        groupbox_files.setLayout(files_vbox)

        self.options_group_box = QtGui.QGroupBox("Options", self)
        self.options_group_box.setStyleSheet(self.groupbox_stylesheet)

        self.search_directories = QtGui.QCheckBox("Search &multiple directories",
            self)
        self.search_directories.setToolTip("If your files are in multiple "
            "directories (e.g. ten seasons in ten folders)\nselecting this "
            "will try to automatically find them.")
        self.search_directories.stateChanged.connect(self.set_multiple_dir_flag)

        # add possibility to change the filenames of video and subtitle file
        self.change_filename_check = QtGui.QCheckBox(
            "Re&name files to...", self)
        self.change_filename_check.setSizePolicy(QtGui.QSizePolicy.Fixed,
            QtGui.QSizePolicy.Fixed)
        self.change_filename_check.setToolTip("Change both the video and "
            "subtitle files to a new filename of your choice")
        # QLineEdit that becomes active on checkbox activated
        self.change_filename_check.stateChanged.connect(
            self.enable_rename_text_edit)

        # create regex to be used by QValidator
        regex = QtCore.QRegExp(r"[\w\(\)\[\]\<\>\.\ \-\+]+")
        self.validator = QtGui.QRegExpValidator(regex, self)

        self.rename_line_edit = QtGui.QLineEdit(self)
        self.rename_line_edit.setSizePolicy(QtGui.QSizePolicy.Expanding,
            QtGui.QSizePolicy.Fixed)
        self.rename_line_edit.setPlaceholderText("new filename")
        self.rename_line_edit.setValidator(self.validator)
        self.rename_line_edit.setEnabled(False)
        if PLATFORM.startswith("linux"):
            self.rename_line_edit.editingFinished.connect(self.set_rename_text)
        elif PLATFORM.startswith("darwin"):
            self.rename_line_edit.textChanged.connect(self.set_rename_text)

        if PLATFORM.startswith("linux"):
            change_filename_hbox = QtGui.QHBoxLayout()
            change_filename_hbox.addWidget(self.change_filename_check)
            change_filename_hbox.addWidget(self.rename_line_edit)

        self.move_checkbox = QtGui.QCheckBox("Move &subtitle files to video "
            "files directory", self)
        self.move_checkbox.setToolTip("Select to merge your subtitle files "
            "with\nyour video files in the video file folder")
        self.move_checkbox.setEnabled(False)
        self.move_checkbox.stateChanged.connect(self.set_copy_flag)

        self.show_details = QtGui.QCheckBox("Show more o&ptions", self)
        self.show_details.setToolTip("Show a dialog screen to preview the "
            "renaming operation\nand to make changes to the selected files")
        self.show_details.stateChanged.connect(self.set_details_flag)
        self.show_details.setChecked(False)

        options_grid = QtGui.QGridLayout()
        options_grid.addWidget(self.search_directories, 0, 0)
        if PLATFORM.startswith("linux"):
            options_grid.addLayout(change_filename_hbox, 1, 0)
        elif PLATFORM.startswith("darwin"):
            options_grid.addWidget(self.change_filename_check, 1, 0)
            options_grid.addWidget(self.rename_line_edit, 1, 1)
        options_grid.addWidget(self.move_checkbox, 2, 0)
        options_grid.addWidget(self.show_details, 3, 0)
        if PLATFORM.startswith("linux"):
            options_grid.setSpacing(12);
        elif PLATFORM.startswith("darwin"):
            options_grid.setSpacing(20);

        self.options_group_box.setLayout(options_grid)

        # exit and ready buttons
        exit_button = QtGui.QPushButton("&Exit", self)
        exit_button.clicked.connect(self.close)

        self.ready_button = QtGui.QPushButton("&Run", self)
        self.ready_button.setEnabled(False)
        self.ready_button.clicked.connect(self.process_files)

        button_hbox = QtGui.QHBoxLayout()
        button_hbox.addWidget(exit_button)
        button_hbox.addStretch(1)
        button_hbox.addWidget(self.ready_button)

        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(groupbox_files)
        vbox.addWidget(self.options_group_box)
        vbox.addLayout(button_hbox)
        vbox.addStretch(1)

        # add extra spacing for Mac OS X
        if PLATFORM.startswith("darwin"):
            vbox.insertWidget(0, create_spacer_widget())
            vbox.insertWidget(2, create_spacer_widget())

        self.main_widget.setLayout(vbox)
        self.setCentralWidget(self.main_widget)

        self.show()

    def closeEvent(self, evnt):
        """Show a dialog when the user wants to close the application."""
        reply = QtGui.QMessageBox.question(self, "Quit", "Are you sure you "
            "want to exit Subvidworks?", QtGui.QMessageBox.Ok |
            QtGui.QMessageBox.Cancel, QtGui.QMessageBox.Cancel)

        if reply == QtGui.QMessageBox.Ok:
            logging.info("User exits program")
            save_filenames_to_file(self.renamed_files)
            evnt.accept()
        else:
            evnt.ignore()

    def donate_dialog(self):
        reply = QtGui.QMessageBox.information(self, "Donate", "If you find Subvidworks"
            " useful, please consider a donation to support the developer and"
            " to ensure its further development.\n\nClick OK to go to PayPal.",
            QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel,
            QtGui.QMessageBox.Ok)

        if reply == QtGui.QMessageBox.Ok:
            QtGui.QDesktopServices.openUrl(QtCore.QUrl("https://www.paypal.com"
                "/cgi-bin/webscr?cmd=_donations&business=K38QBHRUMSY9A&lc=GB"
                "&item_name=Subvidworks&currency_code=EUR&bn=PP%2dDonations"
                "BF%3abtn_donateCC_LG%2egif%3aNonHosted", QtCore.QUrl.StrictMode))

    def enable_target_button(self, state):
        if state:
            self.target_directory_button.setEnabled(True)
            if str(self.target_directory_label.text()):
                self.new_target_flag = True
            try:
                assert self.new_target_flag
            except AssertionError:
                logging.error("Cannot set {0!r} to True".format(
                    "self.new_target_flag"))
        else:
            self.target_directory_button.setEnabled(False)
            self.new_target_flag = False

        self.sort_copy_and_target_flags()

    def sort_copy_and_target_flags(self):
        if self.target_button_checkbox.isChecked():
            self.set_copy_flag(False)
            self.move_checkbox.setChecked(False)
            self.move_checkbox.setEnabled(False)
        else:
            if self.video_directory and self.sub_directory and (
                self.video_directory != self.sub_directory):
                self.move_checkbox.setEnabled(True)

    def set_multiple_dir_flag(self, state):
        self.multiple_dir_flag = bool(state)
        logging.debug("Multiple dir flag was {}".format(("unset", "set")
            [self.multiple_dir_flag]))

    def set_details_flag(self, state):
        self.details_flag = True if state else False
        logging.debug("Detail flag was {}".format(("unset", "set")
            [self.details_flag]))

    def set_copy_flag(self, state):
        self.copy_flag = True if state else False
        logging.debug("Copy flag was {}".format(("unset", "set")
            [self.copy_flag]))

    def select_target_dir(self):
        dirname = QtGui.QFileDialog.getExistingDirectory(self,
            "Choose target directory", os.path.expanduser("~"))

        self.target_directory_label.setText(str(dirname))
        self.new_target_directory = str(dirname)
        logging.debug("New target directory: {!r}".format(
            self.new_target_directory))

        if str(self.target_directory_label.text()):
            self.new_target_flag = True
        else:
            self.new_target_flag = False

        logging.debug("New target flag is {0}".format(
            ("unset", "set")[self.new_target_flag]))

    def about(self):
        """Displays a dialog with information about Subvidworks."""
        # dialog = QtGui.QDialog(self)
        # dialog.setGeometry(100, 100, 500, 200)
        # dialog.setWindowTitle("About Subvidworks")

        # version_number = QtGui.QLabel("Subvidworks version {}".format(VERSION))
        # version_number.setAlignment(QtCore.Qt.AlignHCenter)

        # created_by = QtGui.QLabel("Created by Stefan van den Akker 2014",
        #     dialog)
        # created_by.setAlignment(QtCore.Qt.AlignHCenter)

        # buttons = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok)
        # buttons.setCenterButtons(True)

        # dialog_vbox = QtGui.QVBoxLayout()
        # dialog_vbox.addStretch(1)
        # dialog_vbox.addWidget(version_number)
        # dialog_vbox.addWidget(created_by)
        # dialog_vbox.addStretch(1)
        # dialog_vbox.addWidget(buttons)

        # buttons.accepted.connect(dialog.accept)

        # dialog.setLayout(dialog_vbox)

        # dialog.exec_()

        QtGui.QMessageBox.about(self, "About Subvidworks", 
            "<b>Subvidworks</b><br />Version {0}<br />Created by Stefan van "
                "den Akker, 2014<br /><a href=\"mailto:srvandenakker.dev@gmail.com"
                "\">srvandenakker.dev@gmail.com</a><br /><br />For feature "
                "requests and bug reports: <a href="
                "\"https://github.com/Neftas/subvidworks\">"
                "https://github.com/Neftas/subvidworks</a>".format(VERSION))

    def aboutqt(self):
        """Displays information about Qt and the Qt version used."""
        QtGui.QMessageBox.aboutQt(self, "About Qt")

    def help_window(self):
        dialog = QtGui.QDialog(self)
        dialog.setWindowTitle("Help")

        if PLATFORM.startswith("linux"):
            filename = os.path.join("/", "usr", "share", "python-subvidworks", 
                "data", "help.html")
        elif PLATFORM.startswith("darwin"):
            filename = os.path.join("Applications", "Subvidworks.app",
            "Contents", "MacOS", "help.html")
        else:
            # TODO FOR OTHER PLATFORMS
            return

        help_buttons = QtGui.QDialogButtonBox(dialog)
        help_buttons.setStandardButtons(QtGui.QDialogButtonBox.Ok)

        help_buttons.accepted.connect(dialog.accept)

        view = QtWebKit.QWebView(dialog)
        view.load(QtCore.QUrl(filename))

        help_vbox = QtGui.QVBoxLayout()
        help_vbox.addWidget(view)
        help_vbox.addWidget(help_buttons)

        dialog.setLayout(help_vbox)

        dialog.exec_()

    def get_filename(self, type_file):
        formatted_file_exts = ext_formatting(type_file)

        filename = QtGui.QFileDialog.getOpenFileName(self,
            "Pick a {} file".format(type_file), "{}".format(os.path.expanduser("~")),
            str("{} files ({})".format(type_file.capitalize(), formatted_file_exts)))

        self.head, self.tail = os.path.split(str(filename))

        if type_file == "video":
            self.video_directory, self.tail_video = self.head, self.tail
            self.file_label_video.setText(self.tail_video)
            # if subtitle not chosen, select possible subtitle here
            if not self.file_label_sub.text():
                # get the keyword
                keyword = get_keyword(self.tail_video)

                # create lists of possible matching subtitles in video dir
                # and in ~/Downloads and ~/Videos and - if a keyword matches
                # for a subdirectory in those folders - their subfolders
                downloads_loc = os.path.join(os.path.expanduser("~"), "Downloads")
                download_dirs = [d for d in glob.glob("{0}/*{1}*".format(
                    downloads_loc, keyword)) if os.path.isdir(d)]

                # Linux
                if PLATFORM.startswith("linux"):
                    videos_loc = os.path.join(os.path.expanduser("~"), "Videos")
                # Mac OS X
                elif PLATFORM.startswith("darwin"):
                    videos_loc = os.path.join(os.path.expanduser("~"), "Movies")

                videos_dirs = [d for d in glob.glob("{0}/*{1}*".format(
                    videos_loc, keyword)) if os.path.isdir(d)]
                sub_dir_video_directory = [d for d in glob.glob("{0}/*{1}*".format(
                    self.video_directory, keyword)) if os.path.isdir(d)]
                search_dirs = ([downloads_loc, videos_loc] + download_dirs
                    + videos_dirs + sub_dir_video_directory)

                for folder in search_dirs[:]:
                    try:
                        assert os.path.exists(folder)
                    except AssertionError:
                        logging.warning("{} folder does not exist".format(
                            folder))
                        search_dirs.remove(folder)

                search_dirs.append(self.video_directory)

                consider_files_sub = list()
                exts_sub = sum([all_combinations_exts(ext) for ext in FILE_EXTS["subtitle"]], [])

                complete_break = False
                for folder in search_dirs:
                    for ext in exts_sub:
                        sub_glob_line = "{}/*{}*.{}".format(folder,
                            keyword, ext)
                        consider_files_sub += glob.glob(sub_glob_line)

                    for f in consider_files_sub:
                        if get_season_and_episode(f)[2]:
                            self.sub_directory, self.tail_sub = os.path.split(f)
                            self.file_label_sub.setText(self.tail_sub)
                            complete_break = True
                            break

                    # prevent unneccessary looping                                
                    if complete_break:
                        break

                    consider_files_sub = list()
                            
        else:
            self.sub_directory, self.tail_sub = self.head, self.tail
            self.file_label_sub.setText(self.tail_sub)
            # if video file not chosen, select possible video file here
            if not self.file_label_video.text():
                keyword = get_keyword(self.tail_sub)
                consider_files_vid = list()
                exts_vid = sum([all_combinations_exts(ext) for ext in FILE_EXTS["video"]], [])
                for ext in exts_vid:
                    sub_glob_line = "{}/*{}*.{}".format(self.sub_directory,
                        keyword, ext)
                    consider_files_vid += glob.glob(sub_glob_line)
                for f in consider_files_vid:
                    if get_season_and_episode(f)[2]:
                        self.video_directory, self.tail_video = os.path.split(f)
                        self.file_label_video.setText(self.tail_video)
                        break

        # enable the ready button when all info is entered by user
        if self.file_label_video.text() and self.file_label_sub.text():
            self.ready_button.setEnabled(True)
            if self.video_directory != self.sub_directory and not (
                self.target_button_checkbox.isChecked()):
                self.move_checkbox.setEnabled(True)
        else:
            self.ready_button.setEnabled(False)

    def enable_rename_text_edit(self, state):
        """Sets or unsets the rename flag."""
        if state == QtCore.Qt.Checked:
            self.rename_flag = True
            self.rename_line_edit.setEnabled(True)
            self.rename_line_edit.setFocus()
            logging.debug("Rename flag was set")
        else:
            self.rename_flag = False
            self.rename_line_edit.setEnabled(False)
            logging.debug("Rename flag was unset")

    def set_rename_text(self):
        """Sets the rename text."""
        source = self.sender()
        text = str(source.text())
        self.rename_text = text
        if self.rename_text:
            logging.debug("'self.rename_text' set to {!r}".format(text))
        else:
            logging.debug("'self.rename_text' is empty".format(
                text))

    def process_files(self):
        # catch if rename flag is set, but no text was entered:
        if self.rename_flag and not self.rename_text:
            logging.debug("Rename flag was set, but no rename text was entered.")
            self.rename_flag = False
        if self.file_label_video.text() and self.file_label_sub.text():
            video_name = str(self.file_label_video.text())
            sub_name = str(self.file_label_sub.text())
            
            # video extension
            self.vid_filename, self.vid_ext = os.path.splitext(video_name)
            
            # subtitle extension
            self.sub_filename, self.sub_ext = os.path.splitext(sub_name)
            
            # initialize keywords
            self.vid_keyword = get_keyword(self.vid_filename)
            pretty_vid_keyword = self.vid_keyword
            self.sub_keyword = get_keyword(self.sub_filename)
            pretty_sub_keyword = self.sub_keyword
            
            if not self.details_flag:
                self.update_list(self.vid_keyword, self.sub_keyword)
                if len(self.video_file_list) == len(self.sub_file_list):
                    logging.debug("Quick route was chosen and worked")
                    self.season_episode_check()
                    return
                else:
                    logging.debug("Quick route was chosen but fell through")
                    self.set_details_flag(True)

            # dialog asking if the keyword is okay
            self.kw_dialog = QtGui.QDialog(self)
            self.kw_dialog.resize(600, 800)
            self.kw_dialog.setWindowTitle("Check keyword and files")

            # make sure F1 shortcut keeps working in dialog
            f1_shortcut = QtGui.QShortcut(self.kw_dialog)
            f1_shortcut.setKey(QtGui.QKeySequence("F1"))
            f1_shortcut.setContext(QtCore.Qt.ApplicationShortcut);
            f1_shortcut.activated.connect(self.help_window)

            video_groupbox = QtGui.QGroupBox("Video files")
            video_groupbox.setStyleSheet(self.groupbox_stylesheet)

            sub_groupbox = QtGui.QGroupBox("Subtitle files")
            sub_groupbox.setStyleSheet(self.groupbox_stylesheet)
            
            video_kw_label = QtGui.QLabel("<b>Keyword video files</b>")
            sub_kw_label = QtGui.QLabel("<b>Keyword subtitle files</b>")
            
            video_kw_line_edit = QtGui.QLineEdit()
            video_kw_line_edit.setText(pretty_vid_keyword)
            call_vid_update = lambda x: self.update_list(
                x, self.sub_keyword, "video")
            video_kw_line_edit.textChanged[str].connect(call_vid_update)

            self.no_video_files_label = QtGui.QLabel("")
            self.no_sub_files_label = QtGui.QLabel("")
            self.warning_ne_sub_vid = QtGui.QLabel("")
            self.warning_ne_sub_vid.setObjectName("warning_label")

            self.video_qlist = QtGui.QListWidget(self.kw_dialog)
            self.video_qlist.setObjectName("vid")
            self.video_qlist.setSelectionMode(
                QtGui.QAbstractItemView.ExtendedSelection)
            self.video_qlist.itemSelectionChanged.connect(
                self.enable_remove_button)

            self.sub_qlist = QtGui.QListWidget(self.kw_dialog)
            self.sub_qlist.setObjectName("sub")
            self.sub_qlist.setSelectionMode(
                QtGui.QAbstractItemView.ExtendedSelection)
            self.sub_qlist.itemSelectionChanged.connect(
                self.enable_remove_button)

            sub_kw_line_edit = QtGui.QLineEdit()
            sub_kw_line_edit.setText(pretty_vid_keyword)
            sub_kw_line_edit.setEnabled(False)

            self.cleanup_vid = QtGui.QPushButton("C&lean up", self.kw_dialog)
            self.cleanup_vid.setObjectName("cleanup_vid")
            self.cleanup_vid.clicked.connect(self.cleanup_list)

            self.cleanup_sub = QtGui.QPushButton("Cl&ean up", self.kw_dialog)
            self.cleanup_sub.setObjectName("cleanup_sub")
            self.cleanup_sub.clicked.connect(self.cleanup_list)

            call_update = lambda x: self.update_list(self.vid_keyword, x, "sub")
            sub_kw_line_edit.setEnabled(True)
            sub_kw_line_edit.setText(pretty_sub_keyword)
            sub_kw_line_edit.textChanged[str].connect(call_update)

            self.update_list(self.vid_keyword, self.sub_keyword)

            # buttons for video list
            self.video_list_add_button = QtGui.QPushButton("&Add...",
                self.kw_dialog)
            self.video_list_add_button.clicked.connect(lambda:
                self.add_items("video"))

            self.video_list_remove_button = QtGui.QPushButton(
                "&Remove", self.kw_dialog)
            self.video_list_remove_button.clicked.connect(lambda: 
                self.remove_selected_items(self.video_qlist, "vid"))
            self.video_list_remove_button.setEnabled(False)

            self.video_list_undo_button = QtGui.QPushButton("&Undo", self.kw_dialog)
            self.video_list_undo_button.setObjectName("vid_undo")
            self.video_list_undo_button.setEnabled(False)
            vid_undo_func = lambda: self.append_to(self.video_qlist,
                self.video_file_list, clear_undo=True)
            self.video_list_undo_button.clicked.connect(vid_undo_func)

            video_list_vbox = QtGui.QVBoxLayout()
            video_list_vbox.addStretch(1)
            video_list_vbox.addWidget(self.cleanup_vid)
            video_list_vbox.addWidget(self.video_list_add_button)
            video_list_vbox.addWidget(self.video_list_remove_button)
            video_list_vbox.addWidget(self.video_list_undo_button)

            video_list_hbox = QtGui.QHBoxLayout()
            video_list_hbox.addWidget(self.video_qlist)
            video_list_hbox.addLayout(video_list_vbox)

            video_vbox = QtGui.QVBoxLayout()
            video_vbox.addWidget(video_kw_label)
            video_vbox.addWidget(video_kw_line_edit)
            video_vbox.addWidget(self.no_video_files_label)
            video_vbox.addLayout(video_list_hbox)

            video_groupbox.setLayout(video_vbox)

            # buttons for subtitle list
            self.sub_list_add_button = QtGui.QPushButton("A&dd...",
                self.kw_dialog)
            self.sub_list_add_button.clicked.connect(lambda:
                self.add_items("subtitle"))

            self.sub_list_remove_button = QtGui.QPushButton(
                "Re&move", self.kw_dialog)
            self.sub_list_remove_button.clicked.connect(lambda: 
                self.remove_selected_items(self.sub_qlist, "sub"))
            self.sub_list_remove_button.setEnabled(False)

            self.sub_list_undo_button = QtGui.QPushButton("U&ndo", self.kw_dialog)
            self.sub_list_undo_button.setObjectName("sub_undo")
            self.sub_list_undo_button.setEnabled(False)
            sub_undo_func = lambda x: self.append_to(self.sub_qlist,
                self.sub_file_list, clear_undo=True)
            self.sub_list_undo_button.clicked.connect(sub_undo_func)

            sub_list_vbox = QtGui.QVBoxLayout()
            sub_list_vbox.addStretch(1)
            sub_list_vbox.addWidget(self.cleanup_sub)
            sub_list_vbox.addWidget(self.sub_list_add_button)
            sub_list_vbox.addWidget(self.sub_list_remove_button)
            sub_list_vbox.addWidget(self.sub_list_undo_button)

            sub_list_hbox = QtGui.QHBoxLayout()
            sub_list_hbox.addWidget(self.sub_qlist)
            sub_list_hbox.addLayout(sub_list_vbox)

            sub_vbox = QtGui.QVBoxLayout()
            sub_vbox.addWidget(sub_kw_label)
            sub_vbox.addWidget(sub_kw_line_edit)
            sub_vbox.addWidget(self.no_sub_files_label)
            sub_vbox.addLayout(sub_list_hbox)

            sub_groupbox.setLayout(sub_vbox)

            preview_button = QtGui.QPushButton("&Preview...", self.kw_dialog)
            preview_button.clicked.connect(self.preview_window)

            dialog_button_box = QtGui.QDialogButtonBox(self.kw_dialog)
            dialog_button_box.addButton(preview_button,
                QtGui.QDialogButtonBox.HelpRole)
            dialog_button_box.setStandardButtons(QtGui.QDialogButtonBox.Ok |
                QtGui.QDialogButtonBox.Cancel)
            
            kw_vbox = QtGui.QVBoxLayout()
            kw_vbox.addWidget(video_groupbox)
            kw_vbox.addWidget(sub_groupbox)
            kw_vbox.addWidget(self.warning_ne_sub_vid)
            kw_vbox.addWidget(dialog_button_box)

            if PLATFORM.startswith("darwin"):
                kw_vbox.insertWidget(0, create_spacer_widget())
                kw_vbox.insertWidget(2, create_spacer_widget())

            # change connecting method
            dialog_button_box.accepted.connect(self.season_episode_check)
            dialog_button_box.rejected.connect(self.kw_dialog.reject)

            self.kw_dialog.setLayout(kw_vbox)    

            self.kw_dialog.setModal(True)
            self.kw_dialog.show()

    def add_items(self, type_file):
        """Open a QFileDialog to select one or multiple files. Based on 
        type_file these will then be added to the appropiate list."""
        formatted_file_exts = ext_formatting(type_file)

        filenames = QtGui.QFileDialog.getOpenFileNames(self, 
            "Pick a {} file".format(type_file), "{}".format(os.path.expanduser("~")),
            str("{} files ({})".format(type_file.capitalize(),
            formatted_file_exts)))

        # determine which lists should be used
        pylist = (self.video_file_list, self.sub_file_list)[type_file == "subtitle"]
        qlist = (self.video_qlist, self.sub_qlist)[type_file == "subtitle"]

        for f in filenames:
            if f not in pylist:
                pylist.append(str(f))
            else:
                logging.debug("{} already in list!".format(f))

        self.append_to(qlist, pylist)

    def preview_window(self):
        self.renamed_files = change_vid_sub_filenames(self.video_directory,
            self.sub_directory, self.video_file_list, self.sub_file_list,
            self.copy_flag, self.multiple_dir_flag,
            self.new_target_directory if self.new_target_flag else "",
            self.rename_text if self.rename_flag else "")

        width = 800
        height = 600

        widget = QtGui.QDialog(self.kw_dialog)
        widget.setWindowTitle("Preview")
        widget.resize(width, height)

        # make sure F1 shortcut keeps working in dialog
        f1_shortcut = QtGui.QShortcut(widget)
        f1_shortcut.setKey(QtGui.QKeySequence("F1"))
        f1_shortcut.setContext(QtCore.Qt.ApplicationShortcut);
        f1_shortcut.activated.connect(self.help_window)
        
        video_text_edit = QtGui.QTextEdit()
        video_text_edit.setReadOnly(True)

        sub_text_edit = QtGui.QTextEdit()
        sub_text_edit.setReadOnly(True)

        text_vid = prettify_src_dst_info(self.renamed_files, "video")
        text_sub = prettify_src_dst_info(self.renamed_files, "sub")

        if text_vid:
            label_vid = QtGui.QLabel("Video files to be renamed:")
            video_text_edit.setHtml(text_vid)
        else:
            label_vid = QtGui.QLabel("<b>No video files are "
                "going to be renamed</b>")

        if text_sub:
            label_sub = QtGui.QLabel("Subtitle files to be renamed:")
            sub_text_edit.setHtml(text_sub)
        else:
            label_sub = QtGui.QLabel("<b>No subtitle files are "
                "going to be renamed</b>")

        button_box = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok)
        button_box.accepted.connect(widget.accept)

        preview_vbox = QtGui.QVBoxLayout()
        preview_vbox.addWidget(label_vid)
        if text_vid:
            preview_vbox.addWidget(video_text_edit)
        preview_vbox.addWidget(label_sub)
        if text_sub:
            preview_vbox.addWidget(sub_text_edit)
        preview_vbox.addWidget(button_box)

        widget.setLayout(preview_vbox)

        widget.setModal(True)
        widget.exec_()

    def cleanup_list(self, l):
        source = self.sender()
        name = source.objectName()
        if name == "cleanup_vid":
            length1 = len(self.video_file_list)
            self.video_file_list = choose_dominating_files(self.video_file_list)
            self.append_to(self.video_qlist, self.video_file_list)
            length2 = len(self.video_file_list)
            logging.debug("Video list cleanup was used which resulted in "
                "changes: {}".format("True: {0} before {1} after".format(
                    length1, length2) if length1 != length2 else "False"))
        elif name == "cleanup_sub":
            length1 = len(self.sub_file_list)
            self.sub_file_list = choose_dominating_files(self.sub_file_list)
            self.append_to(self.sub_qlist, self.sub_file_list)
            length2 = len(self.sub_file_list)
            logging.debug("Subtitle list cleanup was used which resulted in "
                "changes: {}".format("True: {0} before {1} after".format(
                    length1, length2) if length1 != length2 else "False"))

    def remove_selected_items(self, qlist, which_list):
        if which_list == "vid":
            # folder = self.video_directory
            pylist = self.video_file_list  
            button = self.video_list_undo_button
            remove_list = self.removed_video_items
        elif which_list == "sub":
            # folder = self.sub_directory
            pylist = self.sub_file_list
            button = self.sub_list_undo_button
            remove_list = self.removed_sub_items
        for item in qlist.selectedItems():
            # find the absolute path of the file that we want deleted
            to_be_removed = [f for f in pylist[:] if f.endswith(
                str(item.text()))][0]
            pylist.remove(to_be_removed)
            remove_list.append(to_be_removed)
            qlist.takeItem(qlist.row(item))
            logging.debug("{!r} was removed from list {!r}".format(
                to_be_removed, which_list))
        self.update_numbers_list()
        button.setEnabled(True)
        
    def update_list(self, vid_kw, sub_kw, which_list="both"):
        # set both keywords
        vid_keyword = case_insensitive_glob(str(vid_kw))
        sub_keyword = case_insensitive_glob(str(sub_kw))

        if which_list in ("video", "both"):
            # find all appropiate video files
            vid_exts = sum([all_combinations_exts(ext) for ext in FILE_EXTS["video"]], [])
            if self.multiple_dir_flag:
                parent_dir_vid = os.path.dirname(self.video_directory)
                if parent_dir_vid == os.path.expanduser("~") or not (
                    parent_dir_vid.startswith(os.path.expanduser("~"))):
                    # that will take too long, so do a shallow scan
                    video_glob_line = "{0}/*{1}*/*{1}*".format(
                        parent_dir_vid, vid_keyword)
                    self.video_file_list = glob.glob(video_glob_line)
                else:
                    self.video_file_list = [os.path.join(dirpath, f)
                        for dirpath, dirnames, files in os.walk(parent_dir_vid)
                        for f in fnmatch.filter(files,
                            '*{0}*'.format(vid_keyword))]
            
            elif not self.multiple_dir_flag or not self.video_file_list:
                video_glob_line = "{0}/*{1}*".format(self.video_directory,
                    vid_keyword)
                self.video_file_list = glob.glob(video_glob_line)

            # filter final list for relevant extensions
            self.video_file_list = [f for f in self.video_file_list[:]
                if f.endswith(tuple(vid_exts))]

        if which_list in ("sub", "both"):
            # find all appriopriate subtitle files
            sub_exts = sum([all_combinations_exts(ext) for ext in FILE_EXTS["subtitle"]], [])
            if self.multiple_dir_flag:
                parent_dir_sub = os.path.dirname(self.sub_directory)
                # don't allow a very long recursive search
                if parent_dir_sub == os.path.expanduser("~") or not (
                    parent_dir_sub.startswith(os.path.expanduser("~"))):
                    sub_glob_line = "{0}/*{1}*/*{1}*".format(
                        parent_dir_sub, sub_keyword)
                    self.sub_file_list = glob.glob(sub_glob_line)
                else:
                    self.sub_file_list = [os.path.join(dirpath, f)
                        for dirpath, dirnames, files in os.walk(parent_dir_sub)
                        for f in fnmatch.filter(files,
                            "*{0}*".format(sub_keyword))]
            
            elif not self.multiple_dir_flag or not self.sub_file_list:
                sub_glob_line = "{0}/*{1}*".format(self.sub_directory,
                    sub_keyword)
                self.sub_file_list = glob.glob(sub_glob_line)

            # filter final list for relevant extensions
            self.sub_file_list = [f for f in self.sub_file_list[:]
                if f.endswith(tuple(sub_exts))]

        if self.details_flag:
            if which_list in ("video", "both"):
                self.append_to(self.video_qlist, self.video_file_list)
            if which_list in ("sub", "both"):
                self.append_to(self.sub_qlist, self.sub_file_list)
            
            self.update_numbers_list()

            try:
                self.reset_undo()
            except AttributeError:
                pass

    def reset_undo(self):
        self.removed_video_items = list()
        self.removed_sub_items = list()
        self.sub_list_undo_button.setEnabled(False)
        self.video_list_undo_button.setEnabled(False)
        logging.debug("All undos were reset")

    def append_to(self, to_qlist, from_list, clear_undo=False):
        "Append entries of video or sub_file_lists to a QList."
        if clear_undo:
            source = self.sender()
            name = source.objectName()
            remove_list = ((self.removed_video_items, self.removed_sub_items)
                [name == "sub_undo"])
            if remove_list:
                from_list.extend(remove_list)
                remove_list[:] = list()
                source.setEnabled(False)
        # remove all items from the list before adding new ones
        to_qlist.clear()
        for elem in from_list:
            head, tail = os.path.split(elem)
            to_qlist.addItem(tail)
        to_qlist.sortItems()
        self.update_numbers_list()

    def enable_remove_button(self):
        source = self.sender()
        name = source.objectName()
        if name == "sub":
            widget = self.sub_qlist
            button = self.sub_list_remove_button
        elif name == "vid":
            widget = self.video_qlist
            button = self.video_list_remove_button
        if not widget.selectedItems():
            button.setEnabled(False)
        else:
            button.setEnabled(True)

    def update_numbers_list(self):
        """Prints the number of files in the video and subtitle lists. Sets a
        warning message when the two don't correspond, and another one when
        the two do."""
        warning_stylsheet = """
            QLabel#warning_label { border: 1px inset lightgrey;
              border-radius: 5px;
              margin: 10px 0px;
              font-weight: bold;
        """
        # if less sub files than video files are found: show warning
        sub_less_greater = (("less than", "greater than")
            [len(self.video_file_list) > len(self.sub_file_list)])
        if len(self.video_file_list) != len(self.sub_file_list):
            self.warning_ne_sub_vid.setText("The number of video files is "
                "{} than the number of subtitle files.".format(sub_less_greater))
            # color the text red(ish)
            self.warning_ne_sub_vid.setStyleSheet(warning_stylsheet + 
                "color: red; }")
        else:
            self.warning_ne_sub_vid.setText("Number of video files corresponds"
                " to the number of subtitle files.")
            self.warning_ne_sub_vid.setStyleSheet(warning_stylsheet + 
                "color: green; }")

        self.no_video_files_label.setText("This keyword matches <b>{0}</b> video files"
            " in the video file directory:".format(len(self.video_file_list)))
        self.no_sub_files_label.setText("This keyword matches <b>{0}</b> subtitle files"
            " in the subtitle directory:".format(len(self.sub_file_list)))

    def season_episode_check(self):
        vid_season, vid_episode, vid_complete_match = get_season_and_episode(
            self.vid_filename)
        sub_season, sub_episode, sub_complete_match = get_season_and_episode(
            self.sub_filename)

        if not self.video_file_list or not self.sub_file_list:
            abort_msg = QtGui.QMessageBox(self.kw_dialog)
            abort_msg.setWindowTitle("Aborting")
            abort_msg.setIcon(QtGui.QMessageBox.Critical)
            abort_msg.setText("<b>Either the video file list or the subtitle"
                " file list is empty.</b>")
            abort_msg.setInformativeText("The operation will be canceled.")
            abort_msg.setStandardButtons(QtGui.QMessageBox.Ok)
            abort_msg.setDefaultButton(QtGui.QMessageBox.Ok)
            abort_msg.setEscapeButton(QtGui.QMessageBox.Ok)
            ret = abort_msg.exec_()

            if ret == QtGui.QMessageBox.Ok:
                return

        if not vid_complete_match or not sub_complete_match:
            abort_msg = QtGui.QMessageBox(self.kw_dialog)
            abort_msg.setWindowTitle("Aborting")
            abort_msg.setIcon(QtGui.QMessageBox.Critical)
            abort_msg.setText("<b>Either the video file or the subtitle file "
                "does not contain any season and episode information.</b>")
            abort_msg.setInformativeText("The operation will be canceled.")
            abort_msg.setStandardButtons(QtGui.QMessageBox.Ok)
            abort_msg.setDefaultButton(QtGui.QMessageBox.Ok)
            abort_msg.setEscapeButton(QtGui.QMessageBox.Ok)
            ret = abort_msg.exec_()

            if ret == QtGui.QMessageBox.Ok:
                return

        for season, episode in ((vid_season, vid_episode),
            (sub_season, sub_episode)):
            if season is None and episode:
                # warning: proceed or cancel?

                season_msg_box = QtGui.QMessageBox(self.kw_dialog)
                continue_button = QtGui.QPushButton("Continue", season_msg_box)
                season_msg_box.setWindowTitle("Warning")
                season_msg_box.setIcon(QtGui.QMessageBox.Warning)
                season_msg_box.setText("<b>The season data could not be"
                    " unambiguously determined from the filename.</b>")
                season_msg_box.setInformativeText("Would you like to"
                    " continue or cancel?")
                season_msg_box.addButton(continue_button,
                    QtGui.QMessageBox.AcceptRole)
                season_msg_box.setStandardButtons(QtGui.QMessageBox.Cancel)
                season_msg_box.setDefaultButton(QtGui.QMessageBox.Cancel)
                season_msg_box.setEscapeButton(QtGui.QMessageBox.Cancel)
                # catch the return value of the buttons
                ret = season_msg_box.exec_()

                if ret == QtGui.QMessageBox.Cancel:
                    return

        if len(self.video_file_list) > len(self.sub_file_list):
            video_gt = QtGui.QMessageBox(self.kw_dialog)
            video_gt.setWindowTitle("Subtitles not complete")
            video_gt.setIcon(QtGui.QMessageBox.Warning)
            video_gt.setText("<b>The number of video files is greater than the"
                " number of subtile files.</b>")
            video_gt.setInformativeText("As a result, not all video files will "
                "have a matching subtitle file.")
            video_gt.setStandardButtons(QtGui.QMessageBox.Cancel |
                QtGui.QMessageBox.Ok)
            video_gt.setDefaultButton(QtGui.QMessageBox.Cancel)
            video_gt.setEscapeButton(QtGui.QMessageBox.Cancel)

            ret = video_gt.exec_()

            if ret == QtGui.QMessageBox.Cancel:
                return

        if self.details_flag:
            self.kw_dialog.hide()

        # FILE RENAMING
        # if the preview was not used, the dictionary renamed_files is empty
        # if not self.renamed_files["video"] or not self.renamed_files["sub"]:
        self.renamed_files = change_vid_sub_filenames(self.video_directory, 
            self.sub_directory, self.video_file_list, self.sub_file_list,
            self.copy_flag, self.multiple_dir_flag,
            self.new_target_directory if self.new_target_flag else "",
            self.rename_text if self.rename_flag else "")

        if not self.renamed_files["video"] and not self.renamed_files["sub"]:
            QtGui.QMessageBox.warning(self, "No files renamed", 
                "No files were renamed, probably because the files are already"
                " in the same directory and properly named.",
                QtGui.QMessageBox.Ok, QtGui.QMessageBox.Ok)
        else:
            # actually move files
            move_files(self.renamed_files)

            self.show_results()

    def show_results(self):
        """Show the result dialog."""
        open_fileman_button = QtGui.QPushButton("&Show files...", self)
        open_fileman_button.setToolTip("Open your filemanager to see the files"
            " that just got changed")
        open_fileman_button.clicked.connect(self.show_files)

        undo_button = QtGui.QPushButton("&Undo...", self)
        undo_button.setToolTip("Undo the changes you just made to your files")

        exit_button = QtGui.QPushButton("&Exit", self)
        exit_button.setToolTip("Exit the application")
        exit_button.clicked.connect(self.close)

        error_button = QtGui.QPushButton("Send error &report...", self)
        error_button.setToolTip("Open up your email client to send an "
            "error report to the developer")
        error_button.clicked.connect(send_error_report)

        ok_button = QtGui.QPushButton("&OK", self)

        # get the Information icon that is also used in QMessageBox
        # create a pixmap out of an QIcon
        style = QtGui.QApplication.style()
        icon = QtGui.QIcon(style.standardIcon(
            style.SP_MessageBoxInformation, None, self))
        pixmap = QtGui.QPixmap(icon.pixmap(100, 100))

        result_dialog = QtGui.QDialog(self)
        result_dialog.setWindowTitle("Result")

        result_text = QtGui.QLabel("<b>{}</b> files were processed.".format(
            NO_FILES_PROCESSED))
        icon_label = QtGui.QLabel(result_dialog)
        icon_label.setPixmap(pixmap)
        icon_label.setSizePolicy(QtGui.QSizePolicy.Fixed,
            QtGui.QSizePolicy.Preferred)

        text_hbox = QtGui.QHBoxLayout()
        text_hbox.addWidget(icon_label)
        text_hbox.addWidget(result_text)

        button_box = QtGui.QHBoxLayout()
        button_box.addWidget(open_fileman_button)
        button_box.addWidget(undo_button)
        button_box.addWidget(error_button)
        button_box.addWidget(exit_button)
        button_box.addWidget(ok_button)

        result_vbox = QtGui.QVBoxLayout()
        result_vbox.addLayout(text_hbox)
        result_vbox.addStretch(1)
        result_vbox.addLayout(button_box)

        undo_button.clicked.connect(lambda: self.undo(result_dialog))
        ok_button.clicked.connect(lambda: self.hide_result_dialog(result_dialog))

        result_dialog.setLayout(result_vbox)

        result_dialog.rejected.connect(self.clear_fields_and_variables)

        result_dialog.setModal(True)
        result_dialog.show()

    def hide_result_dialog(self, result_dialog):
        self.clear_fields_and_variables()
        result_dialog.close()

    def undo(self, result_dialog):
        """Undo the renaming operation. Replace files in their original
        directories and rename them to their original filename."""
        reply = QtGui.QMessageBox.warning(self, "Undo renaming", "Are you sure "
            "you want to undo the renaming?", QtGui.QMessageBox.Ok |
            QtGui.QMessageBox.Cancel, QtGui.QMessageBox.Cancel)

        if reply == QtGui.QMessageBox.Ok:
            replace_files(self.renamed_files, self.new_target_flag)
            self.clear_fields_and_variables()
            result_dialog.close()

            QtGui.QMessageBox.information(self, "Replaced files", 
                "{} files have been replaced to their original "
                "directories and got their old filename back".format(
                    NO_FILES_PROCESSED), 
                QtGui.QMessageBox.Ok, QtGui.QMessageBox.Ok)

    def clear_fields_and_variables(self):
        """Set the default settings for the main window by clearing
        all labels and checkboxes and so forth."""
        # empty chosen video and subtitle filenames 
        self.file_label_video.setText("")
        self.file_label_sub.setText("")

        # clear rename_line_edit
        self.rename_line_edit.setText("")
        self.change_filename_check.setChecked(False)

        # empty list that keeps all removed items
        self.removed_video_items = list()
        self.removed_sub_items = list()

        # set all options back to unchecked
        self.move_checkbox.setChecked(False)
        self.show_details.setChecked(False)
        self.target_button_checkbox.setChecked(False)
        self.target_directory_label.setText("")
        # used to show the correct folder in self.show_files and
        # still be able to reset self.search_directories checkbox
        if self.multiple_dir_flag:
            self.org_search_directories = True
        else:
            self.org_search_directories = False
        self.search_directories.setChecked(False)

    def show_files(self):
        """Show a file manager that opens the folder where the file changes
        took place."""
        if self.copy_flag:
            directory = self.video_directory
        elif self.new_target_flag:
            directory = self.new_target_directory
        else:
            directory = self.sub_directory

        if self.multiple_dir_flag and not self.new_target_flag:
            dirname = os.path.dirname(directory)
            print "Dirname because of multiple dir flag", dirname
            QtGui.QDesktopServices.openUrl(QtCore.QUrl("file:///{}".format(
                dirname), QtCore.QUrl.TolerantMode))
        else:
            QtGui.QDesktopServices.openUrl(QtCore.QUrl("file:///{}".format(
                directory), QtCore.QUrl.TolerantMode))

    def bug_report(self):
        """Show a dialog that presents options of how to send a bug report."""
        dialog = QtGui.QMessageBox(self)
        dialog.setIcon(QtGui.QMessageBox.Information)
        dialog.setWindowTitle("Report a bug")
        dialog.setText("<b>Do you want to submit a bug report?</b>")
        dialog.setInformativeText("You can either report a bug on GitHub "
            "or send an email with the error report.")
        github_button = QtGui.QPushButton("&GitHub", dialog)
        github_button.clicked.connect(go_to_github)
        email_button = QtGui.QPushButton("&Email", dialog)
        email_button.clicked.connect(send_error_report)
        dialog.addButton(github_button, QtGui.QMessageBox.ActionRole)
        dialog.addButton(email_button, QtGui.QMessageBox.ActionRole)
        dialog.setStandardButtons(QtGui.QMessageBox.Cancel)
        dialog.setDefaultButton(QtGui.QMessageBox.Cancel)

        dialog.exec_()

def main():
    app = QtGui.QApplication(sys.argv)
    mw = MainWindow()
    sys.exit(app.exec_())

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    main()
